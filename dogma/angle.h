// This is free and unencumbered software released into the public domain.

#pragma once

// See: https://dogma.dev/Angle/
typedef struct Angle {
  double radians;
} Angle;

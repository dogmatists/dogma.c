// This is free and unencumbered software released into the public domain.

#pragma once

// See: https://dogma.dev/Longitude/
typedef struct Longitude {
  double radians;
} Longitude;

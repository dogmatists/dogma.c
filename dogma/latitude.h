// This is free and unencumbered software released into the public domain.

#pragma once

// See: https://dogma.dev/Latitude/
typedef struct Latitude {
  double radians;
} Latitude;

# Dogma for C

[![Project license](https://img.shields.io/badge/license-Public%20Domain-blue.svg)](https://unlicense.org)
[![C compatibility](https://img.shields.io/badge/c-C11%20%7C%20C18%20%7C%20C2x-blue)](#)
[![Continuous integration](https://github.com/dogmatists/dogma.c/workflows/Continuous%20integration/badge.svg)](https://github.com/dogmatists/dogma.c/actions?query=workflow%3A%22Continuous+integration%22)

## Prerequisites

- [C11][] compiler (for example, [Clang][] 3.1+ or [GCC][] 4.6+)

[C11]:            https://en.wikipedia.org/wiki/C11_(C_standard_revision)
[Clang]:          https://clang.llvm.org
[GCC]:            https://gcc.gnu.org

## Installation

### Installation from Source Code

```bash
$ git clone https://github.com/dogmatists/dogma.c.git

$ cd dogma.c

$ sudo make install
```

## Examples

### Including the header file

```c
#include <dogma.h>
```

### Checking the library version

### Miscellaneous examples

## Reference

### Constants

#### `DOGMA_VERSION_MAJOR`

#### `DOGMA_VERSION_MINOR`

#### `DOGMA_VERSION_PATCH`

### Enums

### Structs

### Functions

## See Also

- [Dogma for C++](https://github.com/dogmatists/dogma.cpp)
